import { Delete, Get, Post, Put, Query, Route, Tags } from "tsoa";
import { IUserController } from "./interfaces";
import { logSuccess, logError, logWarning } from "../utils/logger";

//ORM
import { deleteUserByid, getAllUsers, getKatasFromUser, getUserByid, updateUserByID } from "../domain/orm/User.orm";
import { BasicResponse } from "./types";

@Route("api/users")
@Tags("Usercontroller")
export class UserController implements IUserController {
    
    /**
     * endpoint to retrieve the users in the collection "Users" of DB
     * @param {string} id id of user to retrieve (optional)
     * @returns 
     */
    @Get("/")
    public async getUsers(@Query() page: number, @Query() limit: number, @Query() id?: string): Promise<any> {

        let response: any = '';
        if (id) {
            logSuccess(`[/api/users] Get User by ID ${id}`);

            response = await getUserByid(id);
            
        }
        else {
            logSuccess('[/api/users] Get All Users Request');
            response = await getAllUsers(page, limit);
        }
        return response;

        //return { message: "Hello...??"}
    }
    /**
      * endpoint to delete users by ID in the collection "Users" of DB
      * @param id id of user to delete (optional)
      * @returns message informing if deletion was correct
      */
    @Delete("/")
    public async deleteUser(@Query() id?: string): Promise<any> {

        let response: any = '';
        if (id) {
            logSuccess(`[/api/users] Delete User by ID ${id}`);
            await deleteUserByid(id).then((r) => {
                response = {
                    status: 204,
                    message: `User with id ${id} deleted successfully`
                }
            })
        }
        else {
            logWarning('[/api/users] delete user Requestwithout ID');
            response = {
                status: 400,
                message: 'Please, provide an ID to remove from database'
            }
        }
        return response;

        //return { message: "Hello...??"}
    }
    /**
     * endpoint to delete users by ID in the collection "Users" of DB
     * @param id id of user to delete (optional)
     * @returns message informing if deletion was correct
     */
    @Put("/")
    public async updateUser(id: string, user: any): Promise<any> {

        let response: any = '';
        if (id) {
            logSuccess(`[/api/users] Update User by ID ${id}`);
            await updateUserByID(id, user).then((r) => {
                response = {
                    message: `User with id ${id} updated successfully`
                }
            })
        }
        else {
            logWarning('[/api/users] update user Requestwithout ID');
            response = {
                message: 'Please, provide an ID to update user from database'
            }
        }
        return response;
    }
/**
     * endpoint to retrieve all katas of a users in the collection "Katas" of DB
     * @param {string} id id of user to retrieve (optional)
     * @returns 
     */
@Get("/katas") //users/katas
public async getKatas(@Query() page: number, @Query() limit: number, @Query() id: string): Promise<any> {

    let response: any = '';
    if (id) {
        logSuccess(`[/api/users/katas] Get All Katas from user by id ${id}`);
        response = await getKatasFromUser(page, limit, id);
        
    }
    else{
        logSuccess(`[/api/users/katas] Get All Katas of User without ID`);
        response={
            message: "ID from user is needed"
        }
    }
    return response;

    //return { message: "Hello...??"}
}
}
