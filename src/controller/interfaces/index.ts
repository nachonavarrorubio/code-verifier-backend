import { IUser } from "@/domain/interfaces/IUser.interface";
import { BasicResponse } from "../types";
import { IKata } from "@/domain/interfaces/IKata.interface";


export interface IHelloController {
    getMessage(name?: string): Promise<BasicResponse>
}

export interface IUserController {
    //Read All Users from database or Get user by ID
    getUsers(page: number, limit: number, id?: string): Promise<any>;
    //Get Katas of user
    getKatas(age: number, limit: number, id?: any): Promise<any>;
    //Delete User from database by ID
    deleteUser(id?: string): Promise<any>;
    //Update new user
    updateUser(id: string, user: any): Promise<any>;
}

export interface IAuthController {
    //register Users
    registerUser(user: IUser): Promise<any>;
    loginUser(auth: any): Promise<any>;
}
export interface IKataController {
    //Read All Katas from database or Get kata by ID
    getKatas(page: number, limit: number, id?: string): Promise<any>;
    //Geet all Katas of a user


    //Create new Kata
    createKata(kata: IKata): Promise<any>
    //Delete Kata from database by ID
    deleteKata(id?: string): Promise<any>;
    //Update kata
    updateKata(id: string, kata: IKata): Promise<any>;
}

