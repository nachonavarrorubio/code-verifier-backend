import { Delete, Get, Post, Put, Query, Route, Tags } from "tsoa";
import { IAuthController } from "./interfaces";
import { logSuccess, logError, logWarning } from "../utils/logger";
import { IUser } from "../domain/interfaces/IUser.interface";
import { IAuth } from "../domain/interfaces/IAuth.interface";

//Orm imports
import { registerUser, loginUser, logoutUser, getUserByid } from "../domain/orm/User.orm";

import { AuthResponse, ErrorResponse } from "./types";

@Route("api/auth")
@Tags("AuthController")
export class AuthController implements IAuthController {

    @Post("/register")
    public async registerUser(user: IUser): Promise<any> {
        let response: any = '';
        if (user) {
            logSuccess(`[/api/auth/register] Register new user ${user.email}`);
            await registerUser(user).then((r) => {
                logSuccess(`[/api/auth/register] Created User ${user.email}`);
                response = {
                    message: `User created successfully: ${user.name}`
                }
            });
        }
        else {
            logWarning('[/api/auth/register] register needs User entity');
            response = {
                message: 'User not register: Please, provide a User entity to create one'
            }
        }
        return response;
    }

    @Post("/login")
    //TODO Authenticate user and return a Jsonwebtoken
    public async loginUser(auth: IAuth): Promise<any> {
        let response: AuthResponse | ErrorResponse | undefined;
        if (auth) {
            logSuccess(`[/api/auth/login] Logged user ${auth.email}`);
            let data = await loginUser(auth);
            response = {
                token: data.token,
                message: `Welcome, ${data.user.name} `
            }
        }
        else {
            logWarning('[/api/auth/login] Login needs Auth entity (email & password)');
            response = {
                message: 'Please, provide an email & password to login',
                error: '[AUTH ERROR]: Please, provide an email && password'
            }
        }
        return response;
    }
    /**
         * endpoint to retrieve the users in the collection "Users" of DB
         * Middleware: Validate JWT
         * In headers you must add th x-access-token with a valid JWT
         * @param {string} id id of user to retrieve (optional)
         * @returns 
         */
    @Get("/me")
    public async userData(@Query() id?: string): Promise<any> {

        let response: any = '';
        if (id) {
            logSuccess(`[/api/users] Get User Data by ID ${id}`);
            response = await getUserByid(id);
        }
        
        return response;

    }
    @Post("/logout")
    //TODO Close session of User
    public async logoutUser(): Promise<any> {
        let response: any = '';
        throw new Error("Method not implemented.");
    }




}