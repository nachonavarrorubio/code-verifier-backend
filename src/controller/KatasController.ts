import { Delete, Get, Post, Put, Query, Route, Tags } from "tsoa";
import { IKataController } from "./interfaces";
import { logSuccess, logError, logWarning } from "../utils/logger";

//ORM
import {getAllKatas, getKataByid ,createKata, deleteKataByid, updateKataByID } from "../domain/orm/Kata.orm";
import { IKata } from "@/domain/interfaces/IKata.interface";

@Route("api/katas")
@Tags("KataController")
export class KatasController implements IKataController {
    /**
     * endpoint to retrieve the katas in the collection "Katas" of DB
     * @param {string} id id of kata to retrieve (optional)
     * @returns All katas or kata by id
     */
    @Get("/")
    public async getKatas(@Query() page: number, @Query() limit: number, @Query() id?: string): Promise<any> {

        let response: any = '';
        if (id) {
            logSuccess(`[/api/katas] Get Kata by ID ${id}`);

            response = await getKataByid(id);
            
        }
        else {
            logSuccess('[/api/katas] Get All Katas Request');
            response = await getAllKatas(page, limit);
        }
        return response; }
    
    /**
      * endpoint to delete katas by ID in the collection "Katas" of DB
      * @param id id of kata to delete (optional)
      * @returns message informing if deletion was correct
      */
    @Delete("/")
    public async deleteKata(@Query() id?: string): Promise<any> {

        let response: any = '';
        if (id) {
            logSuccess(`[/api/katas] Delete Kata by ID ${id}`);
            await deleteKataByid(id).then((r) => {
                response = {
                    status: 204,
                    message: `Kata with id ${id} deleted successfully`
                }
            })
        }
        else {
            logWarning('[/api/katas] delete kata Requestwithout ID');
            response = {
                status: 400,
                message: 'Please, provide an ID to remove from database'
            }
        }
        return response;}
     /**
     * endpoint to update katas by ID in the collection "Katas" of DB
     * @param id id of kata to update
     * @returns message informing if updating was correct
     */
     @Put("/")
     public async updateKata(id: string, kata: IKata): Promise<any> {
 
         let response: any = '';
         if (id) {
             logSuccess(`[/api/katas] Update Kata by ID ${id}`);
             await updateKataByID(id, kata).then((r) => {
                 response = {
                     message: `Kata with id ${id} updated successfully`
                 }
             })
         }
         else {
             logWarning('[/api/katas] update kata Requestwithout ID');
             response = {
                 message: 'Please, provide an ID to update kata from database'
             }
         }
         return response;
     }
     @Post("/")
    public async createKata(kata: IKata): Promise<any> {
        let response: any = '';
        if (kata) {
            logSuccess(`[/api/katas] Create new kata ${kata.name}`);
            await createKata(kata).then((r) => {
                logSuccess(`[/api/katas] Created Kata ${kata.name}`);
                response = {
                    message: `Kata created successfully: ${kata.name}`
                }
            });
        }
        else {
            logWarning('[/api/katas] register needs Kata entity');
            response = {
                message: 'Kata not created: Please, provide a Kata entity to create one'
            }
        }
        return response;
    }





}

