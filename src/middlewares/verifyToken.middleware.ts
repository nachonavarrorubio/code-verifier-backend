import jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';
import dotenv from 'dotenv';

//Configuration of envirovent variables
dotenv.config();

//Obtain Secrete key to generate JWT
const secret = process.env.SECRETKEY || 'MYSECRETKEY';



/**
 * 
 * @param {Request} req  Original Request previous middleware of verification
 * @param {Response }res  Response to verification of JWT
 * @param  {NextFunction } next Next function to be executed
 * @returns Errors of verification or Next execution
 */

export const verifyToken = (req: Request, res: Response, next: NextFunction) => {
    //  Check Header from Request for 'x-access-token'

    let token: any = req.headers['x-access-token'];

    //Verify if  jwt is present
    if (!token) {
        return res.status(403).send({
            authenticationError: 'Missing JWT',
            message: 'Not authorized to consume this endpoint'
        })
    }

    //Cheek the token obtained

    jwt.verify(token, secret, (err: any, decoded: any) => {

        if(err){
            return res.status(500).send({
                authenticationError: 'JWT verification failed',
            message: 'Failed to verify JWT token in request'
            })
        }

        //Pass something to next request (id of user || other)

        //if JWT is OK -> Protected Routes will be executed
        
        next();


    })


}