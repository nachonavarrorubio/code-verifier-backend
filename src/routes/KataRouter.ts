import express, { Request, Response } from "express";
import { KatasController } from "../controller/KatasController";
import { logInfo } from "../utils/logger";

// body parser to read BODY from requests
import bodyParser from "body-parser";
import { verifyToken } from "../middlewares/verifyToken.middleware";
import { IKata, KataLevel } from "../domain/interfaces/IKata.interface";
import { Body } from "tsoa";

let jsonParser = bodyParser.json();


//Router from express
let katasRouter = express.Router();

//  http://localhost:8000/api/users?id=
katasRouter.route('/')
    // GET:
    .get(verifyToken, async (req: Request, res: Response) => {
        //Obtain a Query param
        let id: any = req?.query?.id;

        //Pagination params
        let page: any = req?.query?.page || 1;
        let limit: any = req?.query?.limit || 10;


        logInfo(`Query param: ${id}`);
        //Controller Instance to execute method
        const controller: KatasController = new KatasController();
        // Obtain Response
        const response: any = await controller.getKatas(page, limit, id);
        //Send to the client the rsponse
        return res.status(200).send(response);
    })
    // DELETE
    .delete(verifyToken, async (req: Request, res: Response) => {
        //Obtain a Query param
        let id: any = req?.query?.id;
        logInfo(`Query param: ${id}`);
        //Controller Instance to execute method
        const controller: KatasController = new KatasController();
        // Obtain Response
        const response: any = await controller.deleteKata(id);
        //Send to the client the rsponse
        return res.status(200).send(response);
    })
    .put(jsonParser, verifyToken, async (req: Request, res: Response) => {
        //Obtain a Query param
        let id: any = req?.query?.id;
        //Read from body
        let name: any = req?.body?.name;
        let description: any = req?.body?.description || '';
        let level: KataLevel = req?.body?.level || KataLevel.BASIC;
        let intents: number = req?.body?.intents || 0;
        let stars: number = req?.body?.stars || 0;
        let creator: string = req?.body?.creator;
        let solution: string = req?.body?.solution || '';
        let participants: string[] = req?.body?.participants || [];

        if (name && description && level && intents>=0 && stars>=0 && 
            creator && solution && participants) {
                //Controller Instance to execute method
        const controller: KatasController = new KatasController();

        let kata: IKata = {
            name: name,
            description: description,
            level: level,
            intents: intents,
            stars: stars,
            creator: creator,
            solution: solution,
            participants: participants

        }
        // Obtain Response
        const response: any = await controller.updateKata(id, kata);
        //Send to the client the rsponse
        return res.status(200).send(response);

        }
        else{
            return res.status(402).send({
                message: '[ERROR] updating Kata, you need to send all attributes of Kata'
            });

        }

        logInfo(`Query params: ${id},${name},${description}`);
       
    })
    .post(jsonParser, verifyToken, async (req: Request, res: Response) => {
        
        //Read from body
        let name: any = req?.body?.name;
        let description: any = req?.body?.description || '';
        let level: KataLevel = req?.body?.level || KataLevel.BASIC;
        let intents: number = req?.body?.intents || 0;
        let stars: number = req?.body?.stars || 0;
        let creator: string = req?.body?.creator;
        let solution: string = req?.body?.solution || 'Default solution';
        let participants: string[] = req?.body?.participants || [];

        if (name && description && level && intents>=0 && stars>=0 && 
            creator && solution && participants) {
                //Controller Instance to execute method
        const controller: KatasController = new KatasController();

        let kata: IKata = {
            name: name,
            description: description,
            level: level,
            intents: intents,
            stars: stars,
            creator: creator,
            solution: solution,
            participants: participants

        }
        // Obtain Response
        const response: any = await controller.createKata(kata);
        //Send to the client the rsponse
        return res.status(201).send(response);

        }
        else{
            return res.status(402).send({
                message: '[ERROR] Creating Kata, you need to send all attributes of Kata'
            });

        }

        logInfo(`Query params: ${name},${description}`);
       
    })
//Export Hello Router
export default katasRouter;

/**
 * Get Documents => 200 OK
 * Creation Documents =>201 OK
 * Deletion of documents => 200 (Entity) /204 (No return)
 * Update document => 200 (Entity) /204 (No return)
 */
