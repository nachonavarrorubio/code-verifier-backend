import express, { Request, Response } from "express";
import { logInfo } from "../utils/logger";
import { IUser } from "../domain/interfaces/IUser.interface";
import { AuthController } from "../controller/AuthController";
import { IAuth } from "../domain/interfaces/IAuth.interface";
//BCRYPT for passwords
import bcrypt from 'bcrypt';

//Middleware 
import { verifyToken } from "../middlewares/verifyToken.middleware";
//Body parser (Read JSON from Body in request)
import bodyParser from "body-parser";
// Middleware to read JSON in body
let jsonPArser = bodyParser.json();

//Router from express
let authRouter = express.Router();



authRouter.route('/register')

    .post(jsonPArser, async (req: Request, res: Response) => {

        let { name, password, email, age } = req?.body;
        let hashedPassword = '';

        if (name && password && email && age) {

            //Obtain the password in request and cypher
            hashedPassword = bcrypt.hashSync(password, 8)
            let newUser: IUser = {
                name: name,
                email: email,
                password: hashedPassword,
                age: age,
                katas: []
            }
            //Controller Instance to execute method
            const controller: AuthController = new AuthController();
            // Obtain Response
            const response: any = await controller.registerUser(newUser);

            //Send to the client the rsponse
            return res.status(200).send(response);


        }

    })
authRouter.route('/login')

    .post(jsonPArser, async (req: Request, res: Response) => {

        let { password, email } = req?.body;

        if (password && email) {

            //Controller Instance to execute method
            const controller: AuthController = new AuthController();
            let auth: IAuth = {
                email: email,
                password: password
            }

            // Obtain Response
            const response: any = await controller.loginUser(auth);

            //Send to the client the rsponse which includes the JWT to authorize request
            return res.status(200).send(response);


        }
        else {
            //Send to the client the response
            return res.status(400).send({
                message: '[ERROR User Data missing]: no user can be registered'
            });
        }

    })

//route protected by VERiFY TOKEN Middleware
authRouter.route('/me')
    .get(verifyToken, async (req: Request, res: Response) => {
        //Obtain the ID of user to check it's data
        let id: any = req?.query?.id;
        if (id) {
            //Controller Instance to execute method
            const controller: AuthController = new AuthController();
            // Obtain Response
            let response:any=await controller.userData(id);

            return res.status(200).send(response);
        }
        else {
return res.status(401).send({
    message: 'You are not authorized to perform this action'
});
        }

    })


export default authRouter;
