/**
 * Root rooter
 * Redirections to routers
 */

import express, {Request, Response} from 'express';
import helloRouter from './HelloRouter';
import { logInfo} from '../utils/logger';
import UserRouter from './UserRouter';
import authRouter from './AuthRouter';
import katasRouter from './KataRouter';

//server instance
let server=express();

//Router Instance

let rootRouter= express.Router();

//Activate for request to http:/localhost:8000/api

//GET: http:/localhost:8000/api/ 
rootRouter.get('/', (req: Request, res: Response) => {
    logInfo('GET: http:/localhost:8000/api')
    //Send Hello world
    res.send('Welcome to APP Express + TS +Nodemon + Jest+ Swagger + Mongoose')
})

//Redirections to routers & controllers

server.use('/',rootRouter); //http://localhost:8000/api/
server.use('/hello', helloRouter); //http://localhost:8000/api/hello -->HelloRouter

//Add more routes to the app ...
server.use('/users',UserRouter); //http://localhost:8000/api/users -->UserRouter
//Auth routes
server.use('/auth', authRouter); ///http://localhost:8000/api/auth -->AuthRouter
//Katas router
server.use('/katas', katasRouter); ///http://localhost:8000/api/katas -->Katasrouter



export default server;