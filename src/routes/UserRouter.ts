import express, { Request, Response } from "express";
import { UserController } from "../controller/UsersController";
import { logInfo } from "../utils/logger";

// body parser to read BODY from requests
import bodyParser from "body-parser";
import { verifyToken } from "../middlewares/verifyToken.middleware";

let jsonParser = bodyParser.json();


//Router from express
let UserRouter = express.Router();

//  http://localhost:8000/api/users?id=
UserRouter.route('/')
    // GET:
    .get(verifyToken, async (req: Request, res: Response) => {
        //Obtain a Query param
        let id: any = req?.query?.id;

        //Pagination params
        let page: any = req?.query?.page || 1;
        let limit: any = req?.query?.limit || 10;


        logInfo(`Query param: ${id}`);
        //Controller Instance to execute method
        const controller: UserController = new UserController();
        // Obtain Response
        const response: any = await controller.getUsers(page, limit, id);
        //Send to the client the rsponse
        return res.status(200).send(response);
    })
    // DELETE
    .delete(verifyToken, async (req: Request, res: Response) => {
        //Obtain a Query param
        let id: any = req?.query?.id;
        logInfo(`Query param: ${id}`);
        //Controller Instance to execute method
        const controller: UserController = new UserController();
        // Obtain Response
        const response: any = await controller.deleteUser(id);
        //Send to the client the rsponse
        return res.status(200).send(response);
    })
    .put(verifyToken, async (req: Request, res: Response) => {
        //Obtain a Query param
        let id: any = req?.query?.id;
        let name: any = req?.query?.name;
        let email: any = req?.query?.email;
        let age: any = req?.query?.age;
        logInfo(`Query params: ${id},${name},${email},${age}`);
        //Controller Instance to execute method
        const controller: UserController = new UserController();

        let user = {
            name: name,
            email: email,
            age: age,

        }
        // Obtain Response
        const response: any = await controller.updateUser(id, user);
        //Send to the client the rsponse
        return res.status(200).send(response);
    });
//  http://localhost:8000/api/users/katas?id=
UserRouter.route('/katas')
    //GET
    .get(verifyToken, async (req: Request, res: Response) => {
        //Obtain a Query param
        let id: any = req?.query?.id;

        //Pagination params
        let page: any = req?.query?.page || 1;
        let limit: any = req?.query?.limit || 10;

        //Controller Instance to execute method
        const controller: UserController = new UserController();
        // Obtain Response
        const response: any = await controller.getKatas(page, limit, id);
        //Send to the client the rsponse
        return res.status(200).send(response);

    })

//Export User Router
export default UserRouter;

/**
 * Get Documents => 200 OK
 * Creation Documents =>201 OK
 * Deletion of documents => 200 (Entity) /204 (No return)
 * Update document => 200 (Entity) /204 (No return)
 */
