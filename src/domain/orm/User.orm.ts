import { userEntity } from "../entities/User.entity";

import { logSuccess, logError } from "../../utils/logger";
import { error } from "console";
import { IUser } from "../interfaces/IUser.interface";
import { IAuth } from "../interfaces/IAuth.interface";

//Envirovent variables
import dotenv from 'dotenv';

//BCRYPT for passwords
import bcrypt from 'bcrypt';
//JWT
import jwt from "jsonwebtoken";
import { UserResponse } from "../types/UsersResponse";
import { kataEntity } from "../entities/Kata.entity";
import { IKata } from "../interfaces/IKata.interface";

//Configuration of envirovent variables
dotenv.config();

//Obtain Secrete key to generate JWT
const secret = process.env.SECRETKEY || 'MYSECRETKEY';

//PETICIONES CRUD
/**
 * Method to obtain all users from collection "Users" in mongo Server
 */
export const getAllUsers = async (page: number, limit: number,): Promise<any[] | undefined> => {

    try {
        let userModel = userEntity();
        let response: any = {};
        //Search all users (using pagination)
        await userModel.find()
            .limit(limit)
            .skip((page - 1) * limit)
            .select('nombre email age katas')
            .exec().then((users: IUser[]) => {
                response.users = users;
            });

        //Count total documents in collection "Users"
        await userModel.countDocuments().then((total: number) => {

            response.totalPages = Math.ceil(total / limit);
            response.currentPage = page;
        })
        return response;

    }
    catch (error) {
        logError(`[ORM ERROR]: Getting All Users: ${error}`)
    }

}
//GET User By ID
export const getUserByid = async (id: string): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        //Search User By ID
        return await userModel.findById(id).select('name email age katas');
    }
    catch (error) {
        logError(`[ORM ERROR]: Getting User by ID: ${error}`)
    }


}

//Delete User By ID
export const deleteUserByid = async (id: string): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        //Delete User By ID
        return await userModel.deleteOne({ _id: id });
    }
    catch (error) {
        logError(`[ORM ERROR]: Deleting User by ID: ${error}`)
    }


}
// Update User
export const updateUserByID = async (id: any, user: any): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        //Update user
        return await userModel.findByIdAndUpdate(id, user);
    }
    catch (error) {
        logError(`[ORM ERROR]: Update User User: ${error}`)
    }
}
//Register User
export const registerUser = async (user: IUser): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        //Create / Insert new user
        return await userModel.create(user);
    }
    catch (error) {
        logError(`[ORM ERROR]: Creating User: ${error}`)
    }
}
//Login User
export const loginUser = async (auth: IAuth): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        let userFound: IUser | undefined = undefined;
        let token = undefined;
        // Check ifnique email
        await userModel.findOne({ email: auth.email }).then((user: IUser) => {
            userFound = user;
        }).catch((error) => {
            console.error('[ERROR Authentication in ORM]: User not found');
            throw new Error(`[ERROR Authentication in ORM]: User not found: ${error}`);
        });

        //Check if password is valid (compare with bcrypt)
        let validPassword = bcrypt.compareSync(auth.password, userFound!.password);
        if (!validPassword) {
            console.error('[ERROR Authentication in ORM]: Password not valid');
            throw new Error(`[ERROR Authentication in ORM]: Password not valid:`);
        }

        //Generate our JWT
        token = jwt.sign({ email: userFound!.email }, secret, {
            expiresIn: "2h"
        });
        return {
            user: userFound,
            token: token
        };
    }
    catch (error) {
        logError(`[ORM ERROR]: Creating User: ${error}`)
    }
}

/**
 * Method to obtain all Katas of an user from collection "Katas" in mongo Server
 */
export const getKatasFromUser = async (page: number, limit: number, id: string): Promise<any[] | undefined> => {

    try {
        let userModel = userEntity();
        let katasModel = kataEntity();

        let response: any = {};

        await userModel.findById(id).then(async (user: IUser) => {
            
            response.user = user.email;
            await katasModel.find({ "_id": { "$in": user.katas } }).then((katas: IKata[]) => {
                
                response.katas = katas;
            })

        }).catch((error) => {
            logError(`[ORM ERROR]: Obtaining user: ${error}`)
        })

        return response;

    }
    catch (error) {
        logError(`[ORM ERROR]: Getting All Users: ${error}`)
    }

}
//Logout User
export const logoutUser = async (user: IUser): Promise<any | undefined> => {

}
