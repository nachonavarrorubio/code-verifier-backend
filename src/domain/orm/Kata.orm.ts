import { kataEntity } from "../entities/Kata.entity";
import { logSuccess, logError } from "../../utils/logger";
import { error } from "console";
import { IKata } from "../interfaces/IKata.interface";

//Envirovent variables
import dotenv from 'dotenv';

//Configuration of envirovent variables
dotenv.config();

//PETICIONES CRUD
/**
 * Method to obtain all katas from collection "Katas" in mongo Server
 */
export const getAllKatas = async (page: number, limit: number,): Promise<any[] | undefined> => {

    try {
        let kataModel = kataEntity();
        let response: any = {};
        //Search all katas (using pagination)
        await kataModel.find()
            .limit(limit)
            .skip((page - 1) * limit)
            //.select('nombre email age')
            .exec().then((katas: IKata[]) => {
                response.katas = katas;
            });

        //Count total documents in collection "Users"
        await kataModel.countDocuments().then((total: number) => {

            response.totalPages = Math.ceil(total / limit);
            response.currentPage = page;
        })
        return response;

    }
    catch (error) {
        logError(`[ORM ERROR]: Getting All Katas: ${error}`)
    }

}
//GET User By ID
export const getKataByid = async (id: string): Promise<any | undefined> => {
    try {
        let kataModel = kataEntity();

        //Search User By ID
        return await kataModel.findById(id);
    }
    catch (error) {
        logError(`[ORM ERROR]: Getting Kata by ID: ${error}`)
    }


}

//Delete User By ID
export const deleteKataByid = async (id: string): Promise<any | undefined> => {
    try {
        let kataModel = kataEntity();

        //Delete User By ID
        return await kataModel.deleteOne({ _id: id });
    }
    catch (error) {
        logError(`[ORM ERROR]: Deleting Kata by ID: ${error}`)
    }


}
// Create Kata
export const createKata = async (kata: IKata): Promise<any | undefined> => {
    try {
        let kataModel = kataEntity();

        //Create / Insert new kata
        return await kataModel.create(kata);
    }
    catch (error) {
        logError(`[ORM ERROR]: Creating Kata Kata: ${error}`)
    }

}
// Update Kata
export const updateKataByID = async (id: any, kata: IKata): Promise<any | undefined> => {
    try {
        let kataModel = kataEntity();

        //Update Kata
        return await kataModel.findByIdAndUpdate(id, kata);
    }
    catch (error) {
        logError(`[ORM ERROR]: Update Kata User: ${error}`)
    }
}
