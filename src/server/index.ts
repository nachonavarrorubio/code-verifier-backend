import express, { Express, Request, Response } from "express";

//Swagger
import swaggerUi from 'swagger-ui-express';
//Environment Variables
import dotenv from 'dotenv';
// Securtiy
import cors from 'cors';
import helmet from "helmet";

//ToDo HTTPS
//Root Router
import rootRouter from '../routes';
import mongoose from "mongoose";

//Configuration .env file
dotenv.config();

//Create express app
const server: Express = express();
//const port: string | number = process.env.PORT || 8000; (lo tengo ya definido en ./index.ts)
// * Swagger Config and rout
server.use(
    '/docs',
    swaggerUi.serve,
    swaggerUi.setup(undefined, {
        swaggerOptions: {
            url: "/swagger.json",
            explorer: true
        }
    })

)
//* Define SERVER to use "/api" and use rootrouter
// From this point onover: htto://localhost:8000/api/...
server.use(
    '/api',
    rootRouter
)

// static server
server.use(express.static('public'));
//ToDo Mongoose Connection

mongoose.connect('mongodb://localhost:27017/codeverification')
//Security config
server.use(helmet());
server.use(cors());

// content type:
server.use(express.urlencoded({ extended: true, limit: '50mb' }));
server.use(express.json({ limit: '50mb' }));

//* Redirecciones
//http://localhost:8000/ --> http://localhost:8000/api/
server.get('/', (req: Request, res: Response) => {
    res.redirect('/api');
})

export default server;
