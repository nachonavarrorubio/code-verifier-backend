import dotenv from 'dotenv';
import server from './src/server';
import { logError, logSuccess } from './src/utils/logger';
import { error } from 'console';

//Configuration .env file
dotenv.config();

const port: string | number = process.env.PORT || 8000;

// * Execute SERVER

server.listen(port, ()=>{
logSuccess(`[SERVER ON]: running in http://localhost: ${port}/api`)
});

// * Control SERVER ERROR

server.on('error', (error) =>{
    logError(`[SERVER ERROR]: ${error}`);
});
